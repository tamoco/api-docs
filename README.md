# Tamoco API Documentation

This documentation combines all the API descriptions from our 3 tools. SSO, OM API and RPT API.

## Resources

* [Single Sign On (SSO)](./sso/README.md)
* [Events (EVT)](./evt/README.md)
* [Inventory API (INV API)](./invapi/README.md)
* [Event Pull (PULL)](./pull/README.md)
* [Consent](./consent/README.md)

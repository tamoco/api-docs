# Event Pull (Pull)

The Tamoco Event Pull allows us to consume events stored in data files from remote stores such as AWS S3 and Google Cloud Storage.

## Producing a data file

Pull is flexible and can be configured to consume a wide range of data files from TSV, CSV, JSON, XML etc however if you are looking for guidelines we would recommend JSON or TSV.

### JSON data file

If you wish to produce JSON files we would prefer one JSON record per line. See the common supported attributes below for a full list of the data we can accept. We would prefer that you embed common attributes into a key. See the example below:

*Example*

```
{"timestamp": 1515151515,"device": {"device_id": "41c61b0f-8430-4e61-b178-20a72b53247b"},"geo": {"latitude": 51.524696,"longitude": -0.085004,"accuracy": 0.8}, "app_id": 123}
{"timestamp": 1414141414,"device": {"device_id": "067AC0B8-D955-4DC3-B02F-1310170ED92A"},"geo": {"latitude": 40.789465,"longitude": -5.123456,"accuracy": 0.5}, "app_id": 123}
{"timestamp": 1313131313,"device": {"device_id": "B7347432-AC75-47E6-A82E-BE6ACD6470B5"},"geo": {"latitude": 30.132546,"longitude": -10.789465,"accuracy": 0.2}, "app_id": 123}
```

### Text data file

If you wish to produce TEXT file we would prefer either TSV or CSV. You can use the JSON Key values from the common supported attributes as the column headers. See the example below:

```
timestamp,device_id,make,model,os,os_version,hw_version,latitude,longitude,accuracy,app_id,bundle_id,app_name,app_version,sdk_version
1414141414,41c61b0f-8430-4e61-b178-20a72b53247b,Samsung,SGH­T999,Android,5.2.2,5S,51.524696,-0.085004,0.8,451,com.tmc.swoshing,Swoshing Birds,2.3.7,3.3.3
```


### Common Supported Attributes

Below is a list of the common attributes that we consume. If you have other attributes we can add support for these, please let us know.

| Attribute          | Description                                | Example                                  | JSON Key        |
| ---                | ---                                        | ---                                      | ---             |
| timestamp          | Unix Time when the event was collected     | `1414141414`                             | `"timestamp"`   |
| device[device_id]  | A 16-byte unique device IFA (IDFA or AAID) | `"41c61b0f-8430-4e61-b178-20a72b53247b"` | `"device_id"`   |
| device[make]       | The device make                            | `"Samsung"`                              | `"make"`        |
| device[model]      | The device model                           | `"SGH­T999"`                              | `"model"`       |
| device[os]         | The device os                              | `"Android"`                              | `"os"`          |
| device[os_version] | The device os_version                      | `"5.2.2"`                                | `"os_version"`  |
| device[hw_version] | The device hw_version                      | `"5S"`                                   | `"hw_version"`  |
| geo[latitude]      | Latitude coordinate                        | `51.524696`                              | `"latitude"`    |
| geo[longitude]     | Longitude coordinate                       | `-0.085004`                              | `"longitude"`   |
| geo[accuracy]      | Accuracy factor in meters                  | `0.8`                                    | `"accuracy"`    |
| app[app_id]        | The Tamoco APP ID reference                | `451`                                    | `"app_id"`      |
| app[bundle_id]     | Globally unique bundle identifier          | `"com.tmc.swoshing"`                     | `"bundle_id"`   |
| app[app_name]      | Application name                           | `"Swoshing Birds"`                       | `"app_name"`    |
| app[app_version]   | The version of the Application             | `"2.3.7"`                                | `"app_version"` |
| app[sdk_version]   | The version of our SDK you are running     | `"3.3.3"`                                | `"sdk_version"` |

Each of our pulls are highly configurable so if you would prefer to export your values in a different format we can easily add support for most use cases.

### File Names and Frequency

We can support any file names and frequency so long as the file names are unique, however for optimised loading and data checking we suggest you use a file format which includes a company reference and a date/time stamp. e.g.

```
<companyname>-<yyyy><mm><dd>-<hh><mm><ss>
```

which would look like:

```
dataprovider-20171130-093244
```

We suggest that you produce hourly files however we can support larger frequencies. Daily files are acceptable but we would prefer them to be split into multiple files to improve optimisation.

## Storage

You can upload your files to either AWS or GCS. We can either connect to a bucket of your creation and we will ask you to approve our access of our user to the bucket. Alternatively we can create you a bucket on our GCS and either give your user access to the bucket or create you a user which will have create permissions.

*Further reading*

* https://cloud.google.com/storage/docs/
* https://aws.amazon.com/s3/

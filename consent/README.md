# Consent

## Endpoints

* Production `https://consent.tamoco.com/`
* Staging `https://consent-qa.tamoco.com/`

## Resources

* [Consent](./consent.md)

# Consent API

* [URLs](#urls)
* [Routes](#routes)
* [Resources](#resources)

## URLs

* Production: `https://privacy-sdk-api.tamoco.com`
* Staging: `https://privacy-sdk-api-qa.tamoco.com`

## Routes

* [Show Terms](#show-terms)
* [Update Consent](#update-consent)
* [Decode Consent](#decode-consent)
* [Decode Consent (Bulk)](#decode-consent-bulk)
* [Convert Consent](#convert-consent)
* [Convert Consent (Bulk)](#convert-consent-bulk)

### Show Terms

```text
POST /v1/terms_conditions/
```

Payload:

 * **screen** `String`: The Screen name. Optional
 * **persistent_id** `String`: A 16-byte unique device ID from device OS
 * **latitude** `Number`: The Geo Latitude
 * **longitude** `Number`: The Geo Latitude
 * **time_zone** `String`: The device timezone eg.'America/Los_Angeles'
 * **country** `String`: The device country as uppercased 3166-1 alpha-2 code eg.'GB'
 * **ip** `String`: IPv4 IP address
 * **consent_data** `String`: An encoded TM specific Consent String

Response:

 * **Status**:       200 OK
 * **Content-Type**: application/json
 * **Resource**:     [TermsConditionsResponse](#termsconditionsresponse)


Example GDPR:

```shell
curl \
 'https://privacy-sdk-api-qa.tamoco.com/v1/terms_conditions/' \
  -X POST \
  -d '{
   "latitude":53.9560132,
   "longitude":-1.0932532
}' \
  -H 'Authorization: Token xxx/xxx' \
  -H 'Content-Type: application/json'
=>
HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8

{"id":1,"screen":"default","policy_type":1,"headline":"Asking for Consent, Tamoco internal testing 14/19","body":"This is some body example: We would like to share your data with vendors and their analytics partners, to understand what offers may be interest to you. Let these companies with data they already have collected for 6 months with data they already have collected about you to improve their profile of you, including by inferring insights, to show you relevant advertising. (This profile may include location data, habits carrier data....).","version":6,"purposes":[{"id":1,"name":"Information storage and access","description":"The storage of information, or access to information that is already stored, on your device such as advertising identifiers, device identifiers, cookies, and similar technologies.","status":0},{"id":2,"name":"Personalisation","description":"The collection and processing of information about your use of this service to subsequently personalise advertising and/or content for you in other contexts, such as on other websites or apps, over time. Typically, the content of the site or app is used to make inferences about your interests, which inform future selection of advertising and/or content.","status":0}],"features":[{"id":1,"name":"Matching Data to Offline Sources","description":"Combining data from offline sources that were initially collected in other contexts."},{"id":2,"name":"Linking Devices","description":"Allow processing of a user's data to connect such user across multiple devices."}],"vendors":[{"id":32,"name":"MediaMath, Inc.","status":0,"policy_url":"http://www.mediamath.com/privacy-policy/","purpose_ids":[1,2,3,4,5]},{"id":164,"name":"LiquidM Technology GmbH","status":0,"policy_url":"https://liquidm.com/privacy-policy/","purpose_ids":[1,2,3,4,5]}]}
```

Example CCPA:

```shell
curl \
 'https://privacy-sdk-api-qa.tamoco.com/v1/terms_conditions/' \
  -X POST \
  -d '{
   "country":"US",
   "time_zone":"America/Los_Angeles",
   "consent_data": "CgASCwgTEAEY-LrFsIwsIAM"
}' \
  -H 'Authorization: Token xxx/xxx' \
  -H 'Content-Type: application/json'
=>
HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8

{"id":19,"screen":"default","headline":"test","body":"test ccpa","version":1,"surveyed_at":"2018-01-05T11:25:15Z","policy_type":2,"status":3,"purposes":[],"vendors":[]}
```

### Update Consent

```text
POST /v1/consent/
```

Payload:

* **persistent_id*** `String`: A 16-byte unique device ID from device OS
* **device_id** `String`: A 16-byte unique device ID from device OS
* **terms_id*** `Number`: The identifier of the terms
* **app_id** `Number`: Internal app identifier used for audit
* **policy_type** `Number`: The [PolicyType](#policytype) of applicable data privacy policy
* **status** `Number`: The [CCPAStatus](#ccpastatus) of non-GDPR policies
* **version** `Number`: Version of the terms
* **policy*** `Policy`: A [Policy](#policy)

Response:

* **Status**:       200 OK
* **Content-Type**: application/json
* **Resource**:     [UpdateConsentResponse](#updateconsentresponse)


Example GDPR:

```shell
curl -s \
 'https://privacy-sdk-api-qa.tamoco.com/v1/consent/' \
  -X POST \
  -d '{
  "consent_data": "",
  "terms_id": 1,
  "version": 6,
  "device_id": "4763cae5-2275-cae1-f05e-afe98ad8c449",
  "persistent_id": "bcb18156-001a-4db2-b33a-064ba5aeeac8",
  "policy_type":1,
  "policy": {
    "vendor_ids": [32,164],
    "purpose_ids": [1,2]
  },
}' \
  -H 'Authorization: Token xxx/xxx' \
  -H 'Content-Type: application/json'
=>
{"consent_data":"CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ"}
```

Example CCPA:

```shell
curl -s \
 'https://privacy-sdk-api-qa.tamoco.com/v1/consent/' \
  -X POST \
  -d '{
  "consent_data": "",
  "terms_id": 2,
  "version": 4,
  "device_id": "4763cae5-2275-cae1-f05e-afe98ad8c449",
  "persistent_id": "bcb18156-001a-4db2-b33a-064ba5aeeac8",
  "policy": {
    "vendor_ids": [],
    "purpose_ids": []
  },
  "policy_type":2,
  "status":3
}' \
  -H 'Authorization: Token xxx/xxx' \
  -H 'Content-Type: application/json'
=>
{"consent_data":"CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ"}
```

### Decode Consent

```text
POST /consent/decode
```

Payload:

* **consent_data*** `String`: An encoded TM specific Consent String

Response:

 * **Status**:       200 OK
 * **Content-Type**: application/json
 * **Resource**:     [DecodeConsentResponse](#decodeconsentresponse)


Example:

```shell
curl -s \
 'https://privacy-sdk-api-qa.tamoco.com/v1/consent/decode/' \
  -X POST \
  -d '{
  "consent_data": "CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ"
}' \
  -H 'Authorization: Token xxx/xxx' \
  -H 'Content-Type: application/json'
=>
{"policy":{"vendor_ids":[32,164],"purpose_ids":[1,2]},"surveys":[{"terms_id":1,"version":6,"timestamp":"2019-09-13T13:19:57.614Z"}],"updated_at":"2019-09-13T13:19:57.614Z"}
```

### Decode Consent (Bulk)

```text
POST /consent/decode/bulk
```

Headers:

* **Content-Encoding**: gzip - indicates that request body is compressed
* **Accept-Encoding**: gzip - indicates that server may compress response body

Payload (array of objects with following fields):

* **consent_data*** `String`: An encoded TM specific Consent String

Response:

* **Status**:       200 OK
* **Content-Type**: application/json
* **Resource**:     array of [DecodeConsentResponse](#decodeconsentresponse)

Example:

```shell
curl -s \
  'https://privacy-sdk-api-qa.tamoco.com/v1/consent/decode/bulk' \
  -X POST \
  -d '
    [
      {
        "consent_data": "CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ"
      },
      {
        "consent_data": "CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ"
      }
    ]
  ' \
  -H 'Authorization: Token xxx/xxx' \
  -H 'Content-Type: application/json'
=>
[{"policy":{"vendor_ids":[32,164],"purpose_ids":[1,2]},"surveys":[{"terms_id":1,"version":6,"timestamp":"2019-09-13T13:19:57.614Z"}],"updated_at":"2019-09-13T13:19:57.614Z"},{"policy":{"vendor_ids":[32,164],"purpose_ids":[1,2]},"surveys":[{"terms_id":1,"version":6,"timestamp":"2019-09-13T13:19:57.614Z"}],"updated_at":"2019-09-13T13:19:57.614Z"}]
```

### Convert Consent

```text
POST /consent/convert
```

Headers:

* **Content-Encoding**: gzip - indicates that request body is compressed
* **Accept-Encoding**: gzip - indicates that server may compress response body

Parameters

* **consent_data*** `String`: An encoded TM specific Consent String
* **format** `Number`: The [Format](#convertconsentrequest-format) to convert encoded consent (internal format) to

Response:

 * **Status**:       200 OK
 * **Content-Type**: application/json
 * **Resource**:     [ConvertConsentResponse](#convertconsentresponse)


Example:

```shell
curl  \
 'https://privacy-sdk-api-qa.tamoco.com/v1/consent/convert/' \
  -X POST \
  -d '{
  "consent_data": "CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ",
  "format": 1
}' \
  -H 'Authorization: Token xxx/xxx' \
  -H 'Content-Type: application/json'
=>
{"output_data":"BOm0-roOm0-roCSABBENAAAAAGAAmAAA"}
```

### Convert Consent (Bulk)

```text
POST /consent/convert/bulk
```

Parameters (array of objects with following keys):

* **consent_data*** `String`: An encoded TM specific Consent String
* **format** `Number`: The [Format](#convertconsentrequest-format) to convert encoded consent (internal format) to

Response:

* **Status**:       200 OK
* **Content-Type**: application/json
* **Resource**:     array of [ConvertConsentResponse](#convertconsentresponse)

Example:

```shell
curl  \
  'https://privacy-sdk-api-qa.tamoco.com/v1/consent/convert/bulk' \
  -X POST \
  -d '
    [
      {
        "consent_data": "CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ",
        "format": 1
      },
      {
        "consent_data": "CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ",
        "format": 1
      }
    ]
  ' \
  -H 'Authorization: Token xxx/xxx' \
  -H 'Content-Type: application/json'
=>
[{"output_data":"BOm0-roOm0-roCSABBENAAAAAGAAmAAA"},{"output_data":"BOm0-roOm0-roCSABBENAAAAAGAAmAAA"}]
```

## Resources

### TermsConditionsResponse

Attributes:

* **id** `Integer`:  The Terms ID
* **screen** `String`: The Screen  Key
* **headline** `String`: The Headline content
* **body** `String`: The Body content
* **policy_url** `String`: The URL to the policy
* **version** `Number`: The version of the terms
* **surveyed_at** `String`: The date the terms were last presented to the device
* **purposes** `Array<TermsConditionsResponse_Purpose>`: The Purposes which describe how the publisher will use the data
* **features** `Array<TermsConditionsResponse_Feature>`: The Features which describe how the publisher will use the data
* **vendors** `Array<TermsConditionsResponse_Vendor>`: The Vendors which describe how the publisher will use the data
* **policy_type** `Number`: The [PolicyType](#policytype) of applicable data privacy policy
* **status** `Number`: The [CCPAStatus](#ccpastatus) of non-GDPR policies

Example (GDPR):

```json
{
   "id":1,
   "screen":"login",
   "headline":"Asking for Consent, Tamoco internal testing 14/19",
   "body":"This is some body example: We would like to share your data with vendors and their analytics partners, to understand what offers may be interest to you. Let these companies with data they already have collected for 6 months with data they already have collected about you to improve their profile of you, including by inferring insights, to show you relevant advertising. (This profile may include location data, habits carrier data....).",
   "version":6,
   "policy_url":"https://www.tamoco.com/privacy/",
   "policy_type": 1,
   "purposes":[
      {
         "id":1,
         "name":"Information storage and access",
         "description":"The storage of information, or access to information that is already stored, on your device such as advertising identifiers, device identifiers, cookies, and similar technologies.",
         "status":0
      },
      {
         "id":2,
         "name":"Personalisation",
         "description":"The collection and processing of information about your use of this service to subsequently personalise advertising and/or content for you in other contexts, such as on other websites or apps, over time. Typically, the content of the site or app is used to make inferences about your interests, which inform future selection of advertising and/or content.",
         "status":0
      }
   ],
   "features":[
      {
         "id":1,
         "name":"Matching Data to Offline Sources",
         "description":"Combining data from offline sources that were initially collected in other contexts."
      },
      {
         "id":2,
         "name":"Linking Devices",
         "description":"Allow processing of a user's data to connect such user across multiple devices."
      }
   ],
   "vendors":[
      {
         "id":32,
         "name":"MediaMath, Inc.",
         "status":0,
         "policy_url":"http://www.mediamath.com/privacy-policy/",
         "purpose_ids":[
            1,
            2,
            3,
            4,
            5
         ]
      },
      {
         "id":164,
         "name":"LiquidM Technology GmbH",
         "status":0,
         "policy_url":"https://liquidm.com/privacy-policy/",
         "purpose_ids":[
            1,
            2,
            3,
            4,
            5
         ]
      }
   ]
}
```

Example (CCPA):

```json
{
   "id":1,
   "screen":"ccpa",
   "headline":"CCPA Policy",
   "body":"This is some body example: We would like to share your data with vendors and their analytics partners, to understand what offers may be interest to you. Let these companies with data they already have collected for 6 months with data they already have collected about you to improve their profile of you, including by inferring insights, to show you relevant advertising. (This profile may include location data, habits carrier data....).",
   "version":6,
   "policy_url":"https://www.tamoco.com/privacy/",
   "policy_type": 2,
   "status": 3,
   "purposes":[],
   "features":[],
   "vendors":[]
}
```


### TermsConditionsResponse_Purpose

Attributes:

* **id** `Number`: The Purpose ID
* **name** `String`:  The Purpose Name (from IAB)
* **description** `String`: The Purpose description

Example:

```JSON
{
   "id":1,
   "name":"Information storage and access",
   "description":"The storage of information, or access to information that is already stored, on your device such as advertising identifiers, device identifiers, cookies, and similar technologies.",
   "status":0
}
```

### TermsConditionsResponse_Feature

Attributes:

* **id** `Number`: The Feature ID
* **name** `String`:  The Feature Name (from IAB)
* **description** `String`: The Feature description

Example:

```JSON
{
   "id":1,
   "name":"Matching Data to Offline Sources",
   "description":"Combining data from offline sources that were initially collected in other contexts."
}
```

### TermsConditionsResponse_Vendor

Attributes:

* **id** `Number`: The Vendor ID
* **name** `String`: The Vendor Name (from IAB)
* **status** `Number`: The [VendorStatus](#vendorstatus) of Vendor
* **policy_url** `String`: The URL to the Policy
* **purpose_ids**  `Array<Number>`: The associated PurposeIDs
* **feature_ids** `Array<Number>`: The associated FeatureIDs

Example:

```json
{
   "id":164,
   "name":"LiquidM Technology GmbH",
   "status":0,
   "policy_url":"https://liquidm.com/privacy-policy/",
   "purpose_ids":[
      1,
      2,
      3,
      4,
      5
   ],
   "feature_ids":[
      1,
      2,
      3
   ]
}
```

### UpdateConsentResponse

Attributes:

* **consent_data**: `String` Base64 Encoded Consent Data

Example:

```json
{"consent_data":"CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ"}
```

### DecodeConsentResponse

Attributes:

* **policy** `Hash`: The GDPR Policy
* **ccpa_status** `Number`: The CCPA status [CCPAStatus](#consent-status)
* **surveys** `Array<Survey>`: The history of surveys
* **updated_at** `String`: The time the consent data was last updated

Example:

```json
{
   "policy":{
      "vendor_ids":[
         32,
         164
      ],
      "purpose_ids":[
         1,
         2
      ]
   },
   "ccpa_status": 2,
   "surveys":[
      {
         "terms_id":1,
         "version":6,
         "timestamp":"2019-09-13T13:19:57.614Z"
      }
   ],
   "updated_at":"2019-09-13T13:19:57.614Z"
}
```

### ConvertConsentResponse

Attributes:

* **output_data**: `String` converted consent data as string according to requested [Format](#convertconsentrequest-format)

Example:

```json
{"output_data":"BOHGcYONK5Nr8CSABBENAAAAAKAAmACAAOAAkA"}
```

## Enums

* [VendorStatus](#vendorstatus)
* [CCPAStatus](#ccpastatus)
* [PolicyType](#policytype)
* [ConvertConsentRequest.Format](#convertconsentrequest-format)

### VendorStatus

| ID  | Label          | Description                                                   |
| --- | ---            | ---                                                           |
| 0   | UNKNOWN_STATUS | Status is unknown                                             |
| 1   | CONSENTED      | Vendor has been consented                                     |
| 2   | REJECTED       | Terms have been reviewed and the the Vendor was not consented |

### CCPAStatus

| ID  | Label                 | Description                                                          |
| --- | ---                   | ---                                                                  |
| 0   | UNKNOWN_CONSENT       | Consent is unknown, consent remains granted until explicitly revoked |
| 3   | DO_NOT_SELL           | Consent is granted to analyse but not to sell to partners            |

### PolicyType

| ID  | Label                 | Description                          |
| --- | ---                   | ---                                  |
| 0   | UNKNOWN_Policy        | Policy type is unknown               |
| 1   | GDPR                  | Policy type is GDPR                  |
| 2   | CCPA                  | Policy type is CCPA                  |

### ConvertConsentRequest.Format

| ID | Label | Description                                                                                                                                                                                                                     |
| -- | ---   | ---                                                                                                                                                                                                                             |
| 1  | IAB   | [IAB TCF v1.1 format](https://github.com/InteractiveAdvertisingBureau/GDPR-Transparency-and-Consent-Framework/blob/128f722ba3e0878757ae72bb762d925b71f5d5f5/Consent%20string%20and%20vendor%20list%20formats%20v1.1%20Final.md) |

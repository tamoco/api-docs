# Consent Core Proto

```
syntax = "proto3";

package tamoco.consent.core;

import "github.com/gogo/protobuf/gogoproto/gogo.proto";

option go_package = "bitbucket.org/tamoco/consentpb";
option (gogoproto.goproto_getters_all) = false;
option (gogoproto.goproto_stringer_all) = true;
option (gogoproto.goproto_unrecognized_all) = false;
option (gogoproto.sizer_all) = true;
option (gogoproto.marshaler_all) = true;
option (gogoproto.unmarshaler_all) = true;

enum CCPAStatus {
  UNKNOWN_CONSENT = 0;
  DO_NOT_SELL = 3;
}

enum PolicyType {
  UNKNOWN_POLICY = 0;
  GDPR = 1;
  CCPA = 2;
}

// Consent is the stored details of consent including Policy and Surveys
message Consent {
  // The consent opt-in policy
  GDPRPolicy gdpr_policy = 1 [
    (gogoproto.nullable) = false,
    (gogoproto.jsontag) = "policy,omitempty",
    (gogoproto.customname) = "GDPRPolicy"
  ];

  // CCPA Status it the current ccpa status
  CCPAStatus ccpa_status = 4 [ (gogoproto.customname) = "CCPAStatus" ];

  // The set of terms viewed
  repeated Survey surveys = 2 [
    (gogoproto.nullable) = false,
    (gogoproto.jsontag) = "surveys,omitempty"
  ];

  // A UNIX timestamp indicating when consent was last-modified. Required.
  int64 updated_at = 3
      [ (gogoproto.casttype) = "bitbucket.org/tamoco/common.Timestamp" ];
}

// GDPRPolicy is the accepted/rejected combination of a Vendor and Purpose
message GDPRPolicy {
  // The vendor IDs - from consent-api Vendor model.
  repeated int32 vendor_ids = 1 [ (gogoproto.customname) = "VendorIDs" ];

  // The IAB purpose IDs - from https://vendorlist.consensu.org/vendorlist.json.
  repeated int32 purpose_ids = 2 [ (gogoproto.customname) = "PurposeIDs" ];
}

// Survey records when a TermsAndCondition message was reviewed and actioned
message Survey {
  // TermsID
  int64 terms_id = 1 [ (gogoproto.customname) = "TermsID" ];
  // Version the version of the terms
  int64 version = 2;
  // Timestamp
  int64 timestamp = 3 [
    (gogoproto.customname) = "Timestamp",
    (gogoproto.casttype) = "bitbucket.org/tamoco/common.Timestamp"
  ];
}

// Audit
message Audit {
  // EventTS time the service received the audit
  int64 event_ts = 8 [
    (gogoproto.customname) = "EventTS",
    (gogoproto.casttype) = "bitbucket.org/tamoco/common.Timestamp"
  ];

  // EventID unique UUID given at the time of collection, helpful for deduping
  bytes event_id = 9 [
    (gogoproto.customname) = "EventID",
    (gogoproto.casttype) = "bitbucket.org/tamoco/common.UUID"
  ];

  // A 16-byte UUID from device OS
  bytes persistent_id = 1 [
    (gogoproto.customname) = "PersistentID",
    (gogoproto.casttype) = "bitbucket.org/tamoco/common.UUID"
  ];

  // Internal app identifier
  int64 app_id = 2 [ (gogoproto.customname) = "AppID" ];

  // Device ID is a 16-byte unique device IFA (IDFA or AAID)
  bytes device_id = 3 [
    (gogoproto.customname) = "DeviceID",
    (gogoproto.casttype) = "bitbucket.org/tamoco/common.UUID"
  ];

  // Terms ID is the Terms identifier
  int64 terms_id = 6 [ (gogoproto.customname) = "TermsID" ];

  // Version is the Identifier of the Terms accepted/rejected
  int64 version = 7;

  // A new opt-in GDPR policy.
  GDPRPolicy gdpr_policy = 4 [
    (gogoproto.nullable) = false,
    (gogoproto.jsontag) = "policy,omitempty",
    (gogoproto.customname) = "GDPRPolicy"
  ];

  // A set of new opt-out policies. Optional.
  // repeated core.Policy revoke = 5 [(gogoproto.nullable) = false];
  reserved 5; // removed on 02.09.2019

  // CCPAStatus the new status for CCPA policies.
  CCPAStatus ccpa_status = 10 [ (gogoproto.customname) = "CCPAStatus" ];
}

```
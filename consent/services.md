# Consent Services Proto

```
syntax = "proto3";

package tamoco.consent.services;

import "github.com/gogo/protobuf/gogoproto/gogo.proto";
import "corepb/core.proto";
import "bitbucket.org/tamoco/consent-api/cmpb/cm.proto";
import "bitbucket.org/tamoco/evtpb/events.proto";

option go_package = "bitbucket.org/tamoco/consent/servicespb";
option (gogoproto.goproto_getters_all) = false;
option (gogoproto.goproto_stringer_all) = true;
option (gogoproto.goproto_unrecognized_all) = false;
option (gogoproto.sizer_all) = true;
option (gogoproto.marshaler_all) = true;
option (gogoproto.unmarshaler_all) = true;

// V1 API
service V1 {
  rpc DecodeConsent(DecodeConsentRequest)
      returns (DecodeConsentResponse); // POST /consent/decode/
  rpc UpdateConsent(UpdateConsentRequest)
      returns (UpdateConsentResponse); // POST /consent/
  rpc ConvertConsent(ConvertConsentRequest)
      returns (ConvertConsentResponse); // POST /consent/convert/
  rpc GetTermsAndConditions(GetTermsAndConditionsRequest)
      returns (TermsConditionsResponse); // POST /terms_conditions/
}

// DecodeConsentRequest defines encoded consent payload for decoding
// (unpacking).
message DecodeConsentRequest {
  // Consent data (string), as returned by the server...
  string consent_data = 1;
}

// DecodeConsentResponse defines result of decoding consent string into consent
// object.
message DecodeConsentResponse {
  core.Consent consent = 1 [
    (gogoproto.nullable) = false,
    (gogoproto.embed) = true,
    (gogoproto.jsontag) = ""
  ];
}

// UpdateConsentRequest defines consent payload for encoding (packing).
message UpdateConsentRequest {
  // Existing consent data (string), as returned by the server. Optional.
  string consent_data = 1;

  // A 16-byte UUID from device OS. Recommended.
  // Used for Audit event.
  bytes persistent_id = 2 [
    (gogoproto.customname) = "PersistentID",
    (gogoproto.casttype) = "bitbucket.org/tamoco/common.UUID"
  ];

  // Internal app identifier. Recommended.
  // Used for Audit event.
  int64 app_id = 3 [ (gogoproto.customname) = "AppID" ];

  // Device ID is a 16-byte unique device IFA (IDFA or AAID). Recommended.
  // Used for Audit event.
  bytes device_id = 4 [
    (gogoproto.customname) = "DeviceID",
    (gogoproto.casttype) = "bitbucket.org/tamoco/common.UUID"
  ];

  // Terms ID is the Terms identifier. Required.
  int64 terms_id = 5 [ (gogoproto.customname) = "TermsID" ];

  // Version is the Identifier of the Terms accepted/rejected. Required.
  int64 version = 6;

  // PolicyType the applicable data privacy policy
  tamoco.consent.api.PolicyType policy_type = 9;

  // A new opt-in policy. Required.
  core.GDPRPolicy gdpr_policy = 7 [
    (gogoproto.nullable) = false,
    (gogoproto.customname) = "GDPRPolicy",
    (gogoproto.jsontag) = "policy,omitempty"
  ];

  // Status the new status for non Vendor based policy types. Required.
  tamoco.evt.ConsentStatus status = 8;
}

// UpdateConsentResponse defines consent update result.
message UpdateConsentResponse {
  // Consent data (string) to be passed back to server as is.
  string consent_data = 1;
}

// ConvertConsentRequest defines payload to convert existing consent data to
// another format.
message ConvertConsentRequest {
  // Consent data (string), as returned by the server.
  string consent_data = 1;

  // Conversion output format.
  enum Format {
    INVALID_FORMAT = 0;
    IAB = 1;
  }
  Format format = 2;
}

// ConvertConsentResponse defines consent data conversion result.
message ConvertConsentResponse {
  // Converted consent data.
  string output_data = 1;
}

// GetTermsAndConditionsRequest
message GetTermsAndConditionsRequest {
  // Consent data (string), as returned by the server. Optional.
  string consent_data = 1;

  // The App ID. Required.
  int64 app_id = 2 [ (gogoproto.customname) = "AppID" ];

  // The Screen Key. Required.
  string screen = 3;

  // A 16-byte UUID from device OS. Optional.
  bytes persistent_id = 4 [
    (gogoproto.customname) = "PersistentID",
    (gogoproto.casttype) = "bitbucket.org/tamoco/common.UUID"
  ];

  // Latitude coordinate to identify which terms are required. Optional
  double latitude = 5;

  // Longitude coordinate to identify which terms are required. Optional
  double longitude = 6;

  // IPv4 address as 4 bytes for use when location is disabled. Optional
  bytes ip = 7 [
    (gogoproto.customname) = "IP",
    (gogoproto.casttype) = "bitbucket.org/tamoco/common.IPv4",
    (gogoproto.jsontag) = "ip,omitempty"
  ];

  // Country is an uppercased 3166-1 alpha-2 code for use when location is disabled. Optional
  string country = 8;

  // TimeZone the TZ identifier for use when location is disabled. Optional
  string time_zone = 9;
}

enum Status {
  UNKNOWN_STATUS = 0; // Status Is Unknown
  CONSENTED = 1;      // Item has been consented
  REJECTED = 2;       // Terms have been reviewed and item was not consented
}

// TermsConditionsResponse a terms and conditions attributed with device consent
message TermsConditionsResponse {

  // Purpose is an IAB Purpose
  message Purpose {
    // ID
    int32 id = 1 [ (gogoproto.customname) = "ID" ];
    // Name
    string name = 2;
    // Description
    string description = 3;
    // Status
    Status status = 4 [ (gogoproto.jsontag) = "status" ];
  }

  // Feature is an IAB Feature
  message Feature {
    // ID
    int32 id = 1 [ (gogoproto.customname) = "ID" ];
    // Name
    string name = 2;
    // Description
    string description = 3;
  }

  // Vendor is a Vendor
  message Vendor {
    // ID
    int32 id = 1 [ (gogoproto.customname) = "ID" ];
    // Name
    string name = 2;
    // Status
    Status status = 3 [ (gogoproto.jsontag) = "status" ];
    // Policy URL
    string policy_url = 4 [ (gogoproto.customname) = "PolicyURL" ];
    // PurposeIDs
    repeated int32 purpose_ids = 5 [ (gogoproto.customname) = "PurposeIDs" ];
    // FeatureIDs
    repeated int32 feature_ids = 6 [ (gogoproto.customname) = "FeatureIDs" ];
  }

  // ID
  int64 id = 1 [ (gogoproto.customname) = "ID" ];
  // Screen
  string screen = 2;
  // Headline
  string headline = 3;
  // Policy URL
  string policy_url = 10 [ (gogoproto.customname) = "PolicyURL" ];
  // Body
  string body = 4;
  // Version the version of the terms
  int64 version = 6;
  // SurveyedAt when the terms were reviewed by the user
  int64 surveyed_at = 7 [ (gogoproto.casttype) = "bitbucket.org/tamoco/common.Timestamp" ];
  // PolicyType the applicable data privacy policy
  tamoco.consent.api.PolicyType policy_type = 11;
  // Status the current consent status for non Vendor based policy types
  tamoco.evt.ConsentStatus status = 12;

  // Purposes
  repeated Purpose purposes = 5 [ (gogoproto.nullable) = false ];
  // Features
  repeated Feature features = 9 [ (gogoproto.nullable) = false ];
  // Vendors
  repeated Vendor vendors = 8 [ (gogoproto.nullable) = false ];
}

```
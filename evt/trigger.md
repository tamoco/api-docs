## Trigger Data

| Attribute      | Description                                                                                          | Type      | Example         |
| ----           | ---                                                                                                  | ---       | ---             |
| trigger_type   | The defining [Trigger type](#trigger-type)                                                           | `Integer` | `1`             |
| trigger_code   | Trigger Code to uniquely identify the inventory, nearfield triggers only                             | `String`  | `"xxcode11"`    |
| trigger_variant| Trigger variant to uniquely identify the placement of an piece of inventory, nearfield triggers only | `String`  | `"xxvariantxx"` |
| proximity      | Proximity to trigger, in meters                                                                      | `Float`   | `0.8`           |
| battery        | Trigger Battery level (%)                                                                            | `Integer` | `55`            |
| tag_id         | TagID is sometimes sent via the NFC chip for authentication                                          | `String`  | `"xxxtagidxxx"` |
| tac            | TAC is sometimes sent via the NFC chip for authentication                                            | `String`  | `"xxxtacxxx"`   |
| inventory_id   | Inventory ID                                                                                         | `Integer` | `32`            |
| beacon_id      | Beacon ID (16-byte UUID), for Beacon triggers                                                        | `Bytes`   | ``              |
| major          | Beacon major version, for Beacon triggers                                                            | `Integer` | `2`             |
| minor          | Beacon minor version, for Beacon triggers                                                            | `Integer` | `1`             |
| ssid           | SSID, for WIFI triggers                                                                              | `String`  | ``              |
| mac            | MAC, for WIFI triggers                                                                               | `String`  | `a1b2c2a4f243`  |
| namespace      | Namespace for Eddystone                                                                              | `String`  | ``              |
| instance       | Instance for Eddystone                                                                               | `String`  | ``              |
| manufacturer_id | Bluetooth device [manufacturer id](https://www.bluetooth.com/specifications/assigned-numbers/company-identifiers)        | `Integer` | `22`            |
| operator_name  | Wifi operator friendly name published by Passpoint access point                                      | `String`  | `Starbucks`     |
| venue_name     | Wifi venue name published by Passpoint access point                                                  | `String`  | `Heathrow T5`   |
| capabilities   | The authentication, key management, and encryption schemes supported by WiFi access point            | `String`  | `[WPA][ESS]`    |
| center_freq_0  | The center frequency (in MHz), for WiFi triggers                                                     | `Integer` | `22`            |
| center_freq_1  | The center frequency of 2nd segment (in MHz), for WiFi triggers                                      | `Integer` | `33`            |
| channel_width  | The AP Channel bandwidth                                                                             | `Integer` | `44`            |
| frequency      | The primary 20 MHz frequency (in MHz) of the channel over which the client is communicating with     | `Integer` | `12`            |
| tx_power       | The strength of the signal that the sensor produces during the times it is transmitting              | `Integer` | `-12`           |


## Trigger Type

Trigger has a Type attribute which is in the format of an integer ID.

| ID | Label                | Description                                                                                                       |
| -- | ---                  | ---                                                                                                               |
| 0  | UNKNOWN_TRIGGER      | Unknown trigger type                                                                                              |
| 1  | SDK_BACKGROUND_JOB   | Occurs when a device accumulates location history in the background which is not associated to a sensor/inventory |
| 11 | BLE_ENTER            | Occurs when a device enters the beacon's transmission range                                                       |
| 12 | BLE_HOVER            | Occurs when a device remains in proximity to the beacon (within 1 meter) for the required hover_interval period   |
| 13 | BLE_DWELL            | Occurs when a device remains inside the beacon's transmission range for the required dwell_interval period        |
| 14 | BLE_EXIT             | Occurs when a device leaves the beacon's transmission range                                                       |
| 21 | GEO_FENCE_ENTER      | Occurs when a device enters the geo-fence																																					|
| 22 | GEO_FENCE_DWELL      | Occurs when a device remains inside the geo-fence for the required dwell_interval period                          |
| 23 | GEO_FENCE_EXIT       | Occurs when a device leaves the geo-fence	 																																				|
| 31 | WIFI_ENTER           | Occurs when a device enters a WiFi range																																					|
| 32 | WIFI_DWELL           | Occurs when a device remains within a WiFi range for the required dwell_interval period                           |
| 33 | WIFI_EXIT            | Occurs when a device leaves the WiFi range                                                                        |
| 34 | WIFI_CONNECT         | Occurs when a device connects to a WiFi network                                                                   |
| 35 | WIFI_DISCONNECT      | Occurs when a device disconnects from a WiFi network 																															|
| 41 | NFC_TAP              | Occurs when a device taps an NFC Tag 																																							|
| 51 | QR_SCAN              | Occurs when a device scans a QR code 																																							|
| 61 | EDY_ENTER            | Occurs when a device enters the eddystone's transmission range 																										|
| 62 | EDY_HOVER 						| Occurs when a device remains in very close proximity to the Eddystone 																						|
| 63 | EDY_DWELL            | Occurs when a device remains inside the eddystone's transmission range for the required dwell_interval period 	  |
| 64 | EDY_EXIT             | Occurs when a device leaves the eddystone transmission range																											|

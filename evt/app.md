## App Data

| Attribute     | Description                                                                                   | Type      | Example         |
| ----          | ---                                                                                           | ---       | ---             |
| app_id        | Globally unique app (bundle) identifier - DEPRECATED                                          | `String`  | ``              |
| bundle_id     | Globally unique bundle identifier                                                             | `String`  | `com.rov.birds` |
| app_name      | Application name                                                                              | `String`  | `Grumpy Hens`   |
| app_version   | App version                                                                                   | `String`  | `"2.3.7"`       |
| sdk_version   | SDK version                                                                                   | `String`  | `"1.2.1"`       |
| foreground    | Is app operating in foreground                                                                | `Boolean` | `true`          |

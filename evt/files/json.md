# Track Request

```
{
   "event_id":"4763cae5-2275-cae1-f05e-afe98ad8c449",
   "sdk_timestamp":1414141414777,
   "iab_vendor_consent":"BOhoJQiOhoJQiAGABCENCViAAAAn6ADAB6AHAAkI",
   "consent_data":"CgkKAyCkARICAQISCwgBEAYYroWf1tItGK6Fn9bSLQ",
   "device":{
      "device_id":"41c61b0f-8430-4e61-b178-20a72b53247b",
      "ip":"1.2.3.4",
      "device_type":1,
      "make":"Apple",
      "model":"iPhone",
      "locale":"en_GB",
      "os":"IOS",
      "os_version":"7.1.1",
      "hw_version":"6S",
      "nfc":true,
      "battery":{
         "level":95,
         "health":1,
         "saver":true,
         "technology":"li-ion",
         "temperature":25,
         "voltage":4
      },
      "charging":{
         "on":true,
         "source":1
      },
      "time_zone":"Europe/London",
      "carrier":{
         "network_operator_name":"tmobile",
         "network_operator":"26201",
         "network_country_iso":"FR",
         "network_specifier":"abcde",
         "sim_country_iso":"US",
         "sim_operator_name":"vodafone",
         "network_roaming":true,
         "data_activity":1
      }
   },
   "app":{
      "app_id":"com.rovio.angrybirds",
      "bundle_id":"com.rovio.angrybirds",
      "app_name":"Angry Birds",
      "app_version":"2.3.7",
      "sdk_version":"1.2.8",
      "foreground":true
   },
   "geo":{
      "latitude":51.524696,
      "longitude":-0.085004,
      "accuracy":0.8,
      "speed":{
         "value":30,
         "accuracy":0.4
      },
      "altitude":{
         "value":300,
         "accuracy":0.2
      },
      "floor":3,
      "bearing":33.2,
      "motion":{
         "type":4,
         "confidence":80,
         "acceleration":{
            "x":0.1,
            "y":0.2,
            "z":0.3
         },
         "gravity":{
            "x":0.1,
            "y":0.2,
            "z":0.3
         },
         "rotation":{
            "x":0.1,
            "y":0.2,
            "z":0.3
         },
         "ml_predicted":true
      }
   },
   "trigger":{
      "trigger_type":11,
      "inventory_id":127,
      "beacon_id":"e1c27074-aae9-49fa-a6f1-bc715702bdb0",
      "major":2,
      "minor":1,
      "proximity":6.8,
      "rssi":42,
      "manufacturer_id":2,
      "battery":35
   },
   "custom_id":"ABC-123456-EERC"
}
```

# AroundMe Request

```
{
  "event_id": "4763cae5-2275-cae1-f05e-afe98ad8c449",
  "sdk_timestamp": 1414141414,
  "device": {
    "device_id": "41c61b0f-8430-4e61-b178-20a72b53247b",
    "ip": "1.2.3.4",
    "device_type": 1,
    "make": "Apple",
    "model": "iPhone",
    "locale": "en_GB",
    "os": "IOS",
    "os_version": "7.1.1",
    "hw_version": "6S",
    "battery": {
      "level": 95,
      "health": 1,
      "saver": true,
      "technology": "li-ion",
      "temperature": 25,
      "voltage": 4
    },
    "charging": {
      "on": true,
      "source": 1
    },
    "carrier": {
      "country_code":"GB",
      "name":"tmobile"
    },
    "time_zone": "Europe/London",
    "nfc": true,
    "bluetooth": false,
    "location": true,
    "wifi": true
  },
  "app": {
    "app_id": "com.rovio.angrybirds",
    "bundle_id": "com.rovio.angrybirds",
    "app_name": "Angry Birds",
    "app_version": "2.3.7",
    "sdk_version": "1.2.8",
    "foreground": true
  },
  "around_me": [
    {
      "sdk_timestamp": 1414141414,
      "geo": {
        "latitude": 51.524696,
        "longitude": -0.085004,
        "accuracy": 0.8,
        "speed": {
           "value":    30,
           "accuracy": 0.4
        },
        "altitude": {
          "value":    300,
          "accuracy": 0.2
        },
        "floor":   3,
        "bearing": 33.2,
        "pressure": 1002.6,
      },
      "trigger": {
        "trigger_type": 11,
        "inventory_id": 127,
        "beacon_id": "e1c27074-aae9-49fa-a6f1-bc715702bdb0",
        "major": 2,
        "minor": 1,
        "proximity": 6.8,
        "instance": "9995",
        "rssi": 42,
        "manufacturer_id": 2,
        "operator_name": "Starbucks",
        "namespace": "23a01af0232a45189c0e323fb773f5ef",
        "venue_name": "Heathrow (Terminal 5)",
        "capabilities": "[WPA-PSK-TKIP][ESS][WPS]",
        "center_freq_0": 22,
        "center_freq_1": 33,
        "channel_width": 44,
        "frequency": 12,
        "tx_power": -14,
        "mac": "01:17:C5:5B:74:6D"
      }
    }
  ]
}
```

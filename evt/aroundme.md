# Around Me

Around me allows the SDK to push new sensors to us for validations.

## POST

* Request: `POST /v1/aroundme`
* Example: [aroundme-request.json]

## AroundMeRequest

| Attribute                | Description                                                         | Type      | Example         |
| ----                     | ---                                                                 | ---       | ---             |
| around_me                | AroundMe unknown sensors detected by SDK                            | `Array`   | `[]`            |
| event_id                 | A raw 16-byte UUID, generated by the SDK                            | `Bytes`   |                 |
| sdk_timestamp            | UNIX timestamp generated by the SDK at request time                 | `Integer` | `1515151515`    |
| device                   | [Device data]                                                       | `Hash`    | `{}`            |
| app                      | [App data]                                                          | `Hash`    | `{}`            |

## AroundMe

| Attribute                | Description                                                         | Type      | Example         |
| ----                     | ---                                                                 | ---       | ---             |
| geo                      | [Geo data]                                                          | `Hash`    | `{}`            |
| trigger                  | [Trigger data]                                                      | `Hash`    | `{}`            |
| sdk_timestamp            | UNIX timestamp generated by the SDK at scan time                    | `Integer` | `1515151515`    |



[Trigger data]: ./trigger.md#L1
[App data]: ./app.md#L1
[Device data]: ./device.md#L1
[Geo data]: ./geo.md#L1
[aroundme-request.json]: ./files/json.md#L120

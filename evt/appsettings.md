# App Settings

## GET Settings

```
GET /v1/app/settings.json
```

**Params**

You can send a standard track request to the endpoint.

**Response Attributes**

| Attribute                     | Description                                                                                               | Type      | Example            |
| ---------                     | -----------                                                                                               | ---       | -------            |
| barometer                     |                                                                                                           | `Hash`    | `{}`               |
| barometer["enabled"]          | Duration of how long a barometer scan should last (ms)                                                    | `Boolean` | `true`             |
| barometer["scan_duration"]    | Duration of how long a barometer scan should last (ms)                                                    | `Integer` | `5000`             |
| barometer["scan_interval"]    | Time Barometer scans (ms)                                                                                 | `Integer` | `300000`           |
| beacon                        |                                                                                                           | `Hash`    | `{}`               |
| beacon["dwell_delay"]         | How long to wait before recognising someone is dwelling (ms)                                              | `Integer` | `300000`           |
| beacon["hover_delay"]         | How long to wait before recognising someone is hovering (ms)                                              | `Integer` | `300000`           |
| beacon["near_distance"]       | What distance to class as someone is near the beacon. Used to determine hovering (meters)                 | `Integer` | `300000`           |
| beacon["scan_interval"]       | How often to scan for beacons (ms)                                                                        | `Integer` | `300000`           |
| beacon["scan_duration"]       | How long each individual scan should last  (ms)                                                           | `Integer` | `300000`           |
| beacon["report_delay"]        | How long will the hardware buffer bluetooth delayed scan results before communicated them to the SDK (ms) | `Integer` | `300000`           |
| geofence                      |                                                                                                           | `Hash`    | `{}`               |
| geofence["dwell_delay"]       | How long to wait before recognising someone is dwelling (ms)                                              | `Integer` | `300000`           |
| hit                           |                                                                                                           | `Hash`    | `{}`               |
| hit["upload_interval"]        | The time at which the hits will be uploaded to the server (ms)                                            | `Integer` | `300000`           |
| inventory                     |                                                                                                           | `Hash`    | `{}`               |
| inventory["interval"]         | How long to wait between inventory requests (ms)                                                          | `Integer` | `300000`           |
| inventory["ttl"]              | The max period we should hold on to this inventory item before uploading (ms)                             | `Integer` | `300000`           |
| inventory["update_interval"]  | The time between location checks to see if the inventory needs to be updated (ms)                         | `Integer` | `300000`           |
| location                      |                                                                                                           | `Hash`    | `{}`               |
| location["update_time"]       | How long to wait to trigger a location update (and consequently an inventory update) (ms)                 | `Integer` | `300000`           |
| location["update_interval"]   | The time between location checks to see if the inventory needs to be update (ms)                          | `Integer` | `300000`           |
| location["upload_interval"]   | Interval between location uploads to the API (ms)                                                         | `Integer` | `300000`           |
| notifications                 |                                                                                                           | `Array`   | `[]`               |
| notifications["type"]         | Is the notification partner                                                                               | `String`  | `"ROLLBAR"`        |
| notifications["token"]        | Token generally the api token used to pass data to the notification service                               | `String`  | `"xxxAPITOKENxxx"` |
| notifications["level"]        | Level is the log level                                                                                    | `String`  | `"WARNING"`        |
| wifi                          |                                                                                                           | `Hash`    | `{}`               |
| wifi["dwell_delay"]           | How long to wait before recognising someone is dwelling                                                   | `Integer` | `300000`           |
| wifi["scan_interval"]         | Time Wifi scans (ms)                                                                                      | `Integer` | `300000`           |
| request                       |                                                                                                           | `Hash`    | `{}`               |
| request["update_backoff"]     | After a failed request, how long the SDK should wait before trying again (ms)                             | `Integer` | `300000`           |
| settings                      |                                                                                                           | `Hash`    | `{}`               |
| settings["update_interval"]   | The time between settings updates (ms)                                                                    | `Integer` | `300000`           |
| bug_fender_key                | DEPRECATED  the token used to send logging information to bug fender                                      | `String`  | `xxxAPITOKENxxx`   |
| log_level                     | DEPRECATED  bug fender log level can be either warning, error or debug                                    | `String`  | `WARNING`          |
| inventory_interval            | DEPRECATED  how long to wait between inventory requests (ms)                                              | `Integer` | `300000`           |
| inventory_min_update_interval | DEPRECATED  the soonest another inventory request can be sent (ms)                                        | `Integer` | `300000`           |
| request_retry_update_time     | DEPRECATED  after a failed request, how long the SDK should wait before trying again (ms)                 | `Integer` | `300000`           |
| geofence_dwell_delay          | DEPRECATED  how long to wait before recognising someone is "dwelling" (ms)                                | `Integer` | `300000`           |
| wifi_dwell_delay              | DEPRECATED  how long to wait before recognising someone is "dwelling" (ms)                                | `Integer` | `300000`           |
| beacon_dwell_delay            | DEPRECATED  how long to wait before recognising someone is "dwelling" (ms)                                | `Integer` | `300000`           |
| beacon_hover_delay            | DEPRECATED  how long to wait before recognising someone is "hovering" (ms)                                | `Integer` | `300000`           |
| beacon_near_distance          | DEPRECATED  what distance to class as someone is near the beacon. Used to determine hovering (m)          | `Integer` | `500`              |
| location_update_time          | DEPRECATED  how long to wait to trigger a location update (and consequently an inventory update) (ms)     | `Integer` | `300000`           |
| settings_update_time          | DEPRECATED  how often to update the settings (ms)                                                         | `Integer` | `300000`           |
| beacon_scan_interval          | DEPRECATED  how often to scan for beacons (it doesn't continuously scan) (ms)                             | `Integer` | `300000`           |


**Example Response**

```
{
   "barometer":{
      "enabled":true,
      "scan_duration":10000,
      "scan_interval":20000
   },
   "beacon":{
      "dwell_delay":30000,
      "hover_delay":40000,
      "near_distance":50,
      "scan_interval":60000,
      "scan_duration":70000,
      "report_delay":80000
   },
   "geofence":{
      "dwell_delay":90000
   },
   "hit":{
      "upload_interval":100000
   },
   "inventory":{
      "interval":110000,
      "ttl":120000,
      "update_interval":130000
   },
   "location":{
      "update_time":140000,
      "update_interval":150000,
      "upload_interval":160000
   },
   "notifications":[
      {
         "type":"BUG_FENDER",
         "token":"x1nM44hL4PHpZp8XIsoHwcZcSwCNdAFg",
         "level":"WARNING"
      }
   ],
   "wifi":{
      "dwell_delay":170000,
      "scan_interval":180000
   },
   "request":{
      "update_backoff":190000
   },
   "settings":{
      "update_interval":200000
   },
   "inventory_interval":1200,
   "bug_fender_key":"x1nM44hL4PHpZp8XIsoHwcZcSwCNdAFg",
   "log_level":"WARNING",
   "inventory_min_update_interval":660000,
   "request_retry_update_time":600000,
   "default_geofence_dwell_delay":540000,
   "default_wifi_dwell_delay":480000,
   "default_beacon_dwell_delay":420000,
   "default_beacon_hover_delay":360000,
   "beacon_near_distance":5,
   "location_update_time":240000,
   "settings_update_time":180000,
   "beacon_scan_interval":120000,
   "beacon_scan_duration":60000
}
```

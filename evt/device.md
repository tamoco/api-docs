## Device Data

| Attribute           						| Description                                                           | Type      | Example         |
| ----                						| ---                                                                   | ---       | ---             |
| device_id           						| A 16-byte unique device IFA (IDFA or AAID)                            | `Bytes`   |                 |
| lmt                 						| Indicates limit-ad-tracking status                                    | `Boolean` | `false`         |
| ip                  						| IPv4 address as 4 bytes                                               | `Bytes`   |                 |
| device_type         						| The device type (0:UNKNOWN_DEVICE, 1:PHONE, 2:TABLET)                 | `Integer` | `1`             |
| make                						| The device make                                                       | `String`  | `"Samsung"`     |
| model               						| The device model                                                      | `String`  | `"SGH­T999"`     |
| locale              						| The device locale                                                     | `String`  | `"Android"`     |
| os                  						| The OS name                                                           | `String`  | `"en_US"`       |
| os_version          						| The OS Version                                                        | `String`  | `"5.2.2"`       |
| hw_version          						| The HW version                                                        | `String`  | `"5S"`          |
| screen_width        						| The Screen Width                                                      | `Integer` | `1`             |
| screen_height       						| The Screen Height                                                     | `Integer` | `1`             |
| bluetooth           						| Bluetooth State (Active)                                              | `Boolean` | `true`          |
| wifi                						| WiFi State (Active)                                                   | `Boolean` | `true`          |
| location            						| Location Services State (Active)                                      | `Boolean` | `true`          |
| nfc                 						| Nearfield State (Active)                                              | `Boolean` | `true`          |
| time_zone           						| Device time zone (TZ)                                                 | `String`  | `Europe/Paris`  |
| battery             						| Battery data                                                          | `Hash`    | `{}`            |
| battery[level]      						| Battery level (%)                                                     | `Integer` | `80`            |
| battery[health]     						| Battery health (0:UNKNOWN_HEALTH, 1:HEALTHY, 2:UNHEALTHY)             | `Integer` | `1`             |
| battery[saver]      						| Is battery in saver mode                                              | `Boolean` | `true`          |
| battery[technology] 						| Battery technology                                                    | `String`  | `li-ion`        |
| battery[temperature]						| Battery temperature (Centigrade)                                      | `Integer` | `33`            |
| battery[voltage]    						| Battery voltage                                                       | `Integer` | `4`             |
| charging            						| Charging data                                                         | `Hash`    | `{}`            |
| charging[on]        						| Is device charging                                                    | `Boolean` | `true`          |
| charging[source]    						| Device charging source (0:UNKNOWN_SOURCE, 1:AC, 2:USB, 3:WIRELESS)    | `Integer` | `1`             |
| carrier             						| Carrier data                                                          | `Hash`    | `{}`            |
| carrier[data_activity] 					| The type of activity on a data connection 														| `Integer` | `1`             |
| carrier[network_operator] 			| The numeric name (MCC+MNC) of Carrier 																| `String`  | `12345` 				|
| carrier[network_operator_name] 	| The alphabetic name of the network carrier 														| `String` 	| `tmobile` 			|
| carrier[network_country_iso] 		| The ISO country code for the network carrier 													| `String` 	| `GB` 						|
| carrier[network_specifier] 			| The network specifier of the subscription ID 													| `String` 	| `abcde` 				|
| carrier[sim_operator_name] 			| The Service Provider Name (SPN) 																			| `String` 	| `vodafone` 			|
| carrier[sim_country_iso] 				| The ISO country code equivalent for the SIM provider's country code 	| `String` 	| `GB` 						|
| carrier[network_roaming] 				| True if the device is considered roaming on the current network 			| `Boolean` | `true` 					|

**Device Type**

Device has a Type attribute which is in the format of an integer ID.

| ID | Label             | Description                                                         |
| -- | ---               | ---                                                                 |
| 0  | UNKNOWN_DEVICE    | Unknown device type                                                 |
| 1  | PHONE             | Device is a phone                                                   |
| 2  | TABLET            | Device is a tablet                                                  |

**Battery Health**

Device Battery has a Health attribute which is in the format of an integer ID.

| ID | Label             | Description                                                         |
| -- | ---               | ---                                                                 |
| 0  | UNKNOWN_HEALTH    | The battery health is unknown                                       |
| 1  | HEALTHY           | The battery is in a healthy state                                   |
| 2  | UNHEALTHY         | The battery is in an unhealthy/failed/dead state                    |

**Charging Source**

Device Charging has a Source attribute which is in the format of an integer ID.

| ID | Label             | Description                                                         |
| -- | ---               | ---                                                                 |
| 0  | UNKNOWN_SOURCE    | The charging source is unknown                                      |
| 1  | AC                | The device is plugged into AC outlet                                |
| 2  | USB               | The device is charging via USB                                      |
| 3  | WIRELESS          | The device is charging via wireless dock                            |

## Geo Data

| Attribute           | Description                                                                                       | Type      | Example         |
| ----                | ---                                                                                               | ---       | ---             |
| latitude            | Latitude coordinate                                                                               | `Float`   | `51.524696`     |
| longitude           | Longitude coordinate                                                                              | `Float`   | `-0.085004`     |
| accuracy            | Accuracy factor                                                                                   | `Float`   | `0.8`           |
| floor               | Building floor number                                                                             | `Integer` | `2`             |
| bearing             | Direction of travel in degrees east of true north                                                 | `Float`   | `45.9`          |
| speed               | Speed data                                                                                        | `Hash`    | `{}`            |
| speed[value]        | Speed value in m/s over ground                                                                    | `Float`   | `12.6`          |
| speed[accuracy]     | Accuracy of speed value in m/s                                                                    | `Float`   | `2.2`           |
| altitude            | Altitude data                                                                                     | `Hash`    | `{}`            |
| altitude[value]     | Altitude in metres                                                                                | `Float`   | `20.2`          |
| altitude[accuracy]  | Accuracy of altitude value in metres                                                              | `Float`   | `3.2`           |
| pressure            | Pressure the atmospheric pressure if available, in hPa (millibar)                                 | `Float`   | `1002.2`        |
| motion              | Motion data 																																											| `Hash`    | `{}`            |
| motion[type]        | Type of motion                                                                                    | `Integer` | `1`             |
| motion[confidence]  | Confidence value (%) of motion type validity                                                      | `Integer` | `90`            |

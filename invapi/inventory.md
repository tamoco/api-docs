# Inventory

Inventory refers to the different types of location sensors that our SDK can monitor.

**Type Reference**

All Inventory has a Type attribute which is in the format of a number.

| ID | Label             | Description                                                         |
| -- | ---               | ---                                                                 |
| 0  | UNKNOWN_INVENTORY | The inventory type is unknown                                       |
| 1  | BEACON            | Bluetooth low energy proximity sensing                              |
| 2  | GEO_FENCE         | A geo-fence is a virtual perimeter for a real-world geographic area |
| 3  | WIFI              | A WiFi router                                                       |
| 5  | QR                | A Quick Response Code                                               |
| 6  | NFC               | Near Field Communication                                            |
| 7  | EDDYSTONE         | Bluetooth Low Energy beacon profile released by Google              |

**Status Reference**

| ID | Label    | Description                                                                      |
| -- | ---      | ---                                                                              |
| 0  | PENDING  | Awaiting approval                                                                |
| 1  | ACTIVE   | The inventory is being scanned by our SDK and can be targeted                    |
| 2  | INACTIVE | The inventory has been deactivated and can not be targeted or tracked by our SDK |


## List Inventory

```
GET /v1/inventory?account_id=:account_id
```

**Params**

| Attribute  | Description                       | Type              | Example | Required |
| ---------  | -----------                       | ---               | ------- | ---      |
| account_id | The associated Account ID         | `<Number>`        | `1`     | true     |
| page       | The page number, default 1        | `<Number>`        | `1`     | false    |
| per_page   | The records per page, default 25  | `<Number>`        | `30`    | false    |
| sort       | The column to sort by, id or name | `<String>`        | `id`    | false    |
| filter     | Filters                           | `<Array<String>>` | ``      | false    |

**Response Attributes**

| Attribute              | Description                                                                                                                                                                | Type       | Example                                  |
| ---------              | -----------                                                                                                                                                                | ---        | -------                                  |
| inv["id"]              | The unique ID                                                                                                                                                              | `<Number>` | `279657`                                 |
| inv["account_id"]      | The associated Account by ID                                                                                                                                               | `<Number>` | `101`                                    |
| inv["type"]            | The Inventory Type. See Type Reference                                                                                                                                     | `<Number>` | `1`                                      |
| inv["name"]            | The Inventory Name                                                                                                                                                         | `<String>` | `"Inventory1"`                           |
| inv["latitude"]        | The Inventory Latitude                                                                                                                                                     | `<Number>` | `51.518908`                              |
| inv["longitude"]       | The Inventory Longitude                                                                                                                                                    | `<Number>` | `-0.128532`                              |
| inv["time_zone"]       | The Inventory Time Zone                                                                                                                                                    | `<String>` | `"Europe/London"`                        |
| inv["status"]          | The Inventory Status. See Status Reference                                                                                                                                 | `<Number>` | `1`                                      |
| inv["private"]         | Boolean value, when true the Inventory can only be targeted by the account owner                                                                                           | `<Number>` | `true`                                   |
| inv["dwell_interval"]  | This is the amount of time (in seconds) before a dwell based Event is triggered.                                                                                           | `<Number>` | `300`                                    |
| inv["external_id"]     | An arbitrary ID to associate to the Inventory                                                                                                                              | `<String>` | `"external_inv1"`                        |
| inv["place_id"]        | The associate Place ID                                                                                                                                                     | `<Number>` | 12                                       |
| inv["ttl"]             | TTL is the maximum value in seconds an event triggered on the inventory should be stored on the device before sending to evt, if the value is 0 then the TTL is indefinate | `<Number>` | 300                                      |
| *Beacon*               |                                                                                                                                                                            |            |                                          |
| inv["hover_interval"]  | This is the amount of time (in seconds) before a hover based Event is triggered.                                                                                           | `<Number>` | `300`                                    |
| inv["uuid"]            | The Universally Unique Identifier.                                                                                                                                         | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` |
| inv["major"]           | The major value of the Beacon.                                                                                                                                             | `<Number>` | `7`                                      |
| inv["minor"]           | The minor value of the Beacon.                                                                                                                                             | `<Number>` | `1`                                      |
| *Eddystone*            |                                                                                                                                                                            |            |                                          |
| inv["uuid"]            | The Universally Unique Identifier.                                                                                                                                         | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` |
| *Geofence*             |                                                                                                                                                                            |            |                                          |
| inv["radius"]          | In meters                                                                                                                                                                  | `<Number>` | `50`                                     |
| *WiFi*                 |                                                                                                                                                                            |            |                                          |
| inv["ssid"]            | The Service Set Identifier                                                                                                                                                 | `<Number>` | `HotSpot 123`                            |
| inv["mac"]             | The Mac address of the hardware                                                                                                                                            | `<String>` | `"a1b2c3d4e5f6"`                         |
| *QR/NFC*               |                                                                                                                                                                            |            |                                          |
| inv["destination_url"] | Default destination url                                                                                                                                                    | `<String>` | `"https://example.com/"`                 |
| inv["code"]            | Unique random generated 10 char code                                                                                                                                       | `<String>` | `"orsw43dfor2gk4tt"`                     |

**Header Response**

| Attribute     | Description                                         | Type       | Example |
| ---------     | -----------                                         | ---        | ------- |
| X-Page        | The current Page                                    | `<Number>` | `1`     |
| X-Per-Page    | The records Per Page                                | `<Number>` | `2`     |
| X-Total-Count | The total number of records which match the request | `<Number>` | `10771` |

**Example**

```
curl -sG \
 'https://inv-api.tamoco.com/v1/inventory' \
 -d 'page=1' \
 -d 'per_page=2' \
 -d 'account_id=10' \
 -H 'authorization: Bearer ********'
=>
HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8
X-Page: 1
X-Per-Page: 2
X-Total-Count: 10771

[{"id":279657,"account_id":10,"name":"100 A Heineken test office","latitude":51.518908,"longitude":-0.128532,"time_zone":"Europe/London","status":1,"dwell_time":900,"hover_time":900,"radius":100,"type":2},{"id":279656,"account_id":10,"name":"100 A Heineken Office","latitude":51.517323,"longitude":-0.139867,"time_zone":"Europe/London","status":1,"dwell_time":300,"hover_time":300,"radius":150,"type":2}]

```

## Get Inventory

```
GET /v1/inventory/:id?account_id=:account_id
```

**Params**

| Attribute  | Description               | Type       | Example | Required |
| ---------  | -----------               | ---        | ------- | ---      |
| account_id | The associated Account ID | `<Number>` | `1`     | true     |
| id         | The Inventory ID          | `<Number>` | `1`     | true     |

**Response Attributes**

| Attribute       | Description                                                                      | Type       | Example                                  |
| ---------       | -----------                                                                      | ---        | -------                                  |
| id              | The unique ID                                                                    | `<Number>` | `279657`                                 |
| account_id      | The associated Account by ID                                                     | `<Number>` | `101`                                    |
| type            | The Inventory Type. See Type Reference                                           | `<Number>` | `1`                                      |
| name            | The Inventory Name                                                               | `<String>` | `"Inventory1"`                           |
| latitude        | The Inventory Latitude                                                           | `<Number>` | `51.518908`                              |
| longitude       | The Inventory Longitude                                                          | `<Number>` | `-0.128532`                              |
| time_zone       | The Inventory Time Zone                                                          | `<String>` | `"Europe/London"`                        |
| status          | The Inventory Status. See Status Reference                                       | `<Number>` | `1`                                      |
| private         | Boolean value, when true the Inventory can only be targeted by the account owner | `<Number>` | `true`                                   |
| dwell_interval  | This is the amount of time (in seconds) before a dwell based Event is triggered. | `<Number>` | `300`                                    |
| external_id     | An arbitrary ID to associate to the Inventory                                    | `<String>` | `"external_inv1"`                        |
| place_id        | The associate Place ID                                                           | `<Number>` | 12                                       |
| ttl             | TTL is the maximum value in seconds an event triggered on the inventory should be stored on the device before sending to evt, if the value is 0 then the TTL is indefinate | `<Number>` | 300                                      |
| *Beacon*        |                                                                                  |            |                                          |
| hover_interval  | This is the amount of time (in seconds) before a hover based Event is triggered. | `<Number>` | `300`                                    |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` |
| major           | The major value of the Beacon.                                                   | `<Number>` | `7`                                      |
| minor           | The minor value of the Beacon.                                                   | `<Number>` | `1`                                      |
| *Eddystone*     |                                                                                  |            |                                          |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` |
| *Geofence*      |                                                                                  |            |                                          |
| radius          | In meters                                                                        | `<Number>` | `50`                                     |
| *WiFi*          |                                                                                  |            |                                          |
| ssid            | The Service Set Identifier                                                       | `<Number>` | `HotSpot 123`                            |
| mac             | The Mac address of the hardware                                                  | `<String>` | `"a1b2c3d4e5f6"`                         |
| *QR/NFC*        |                                                                                  |            |                                          |
| destination_url | Default destination url                                                          | `<String>` | `"https://example.com/"`                 |
| code            | Unique random generated 10 char code                                             | `<String>` | `"orsw43dfor2gk4tt"`                     |

**Example**

```
curl -sG \
  'https://inv-api.tamoco.com/v1/inventory/279657?&account_id=10' \
  -H 'authorization: Bearer ********'
=>
HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8


{"id":279657,"account_id":10,"name":"100 A Heineken test office","latitude":51.518908,"longitude":-0.128532,"time_zone":"Europe/London","status":1,"dwell_time":900,"hover_time":900,"radius":100,"type":2}

```

## Create Inventory

```
POST /v1/inventory?account_id=:account_id
```

| Attribute       | Description                                                                      | Type       | Example                                  | Required |
| ---------       | -----------                                                                      | ---        | -------                                  | ---      |
| account_id      | The associated Account ID                                                        | `<Number>` | `1`                                      | true     |
| type            | The Inventory Type. See Type Reference                                           | `<Number>` | `1`                                      | true     |
| name            | The Inventory Name                                                               | `<String>` | `"Inventory1"`                           | true     |
| latitude        | The Inventory Latitude                                                           | `<Number>` | `51.518908`                              | true     |
| longitude       | The Inventory Longitude                                                          | `<Number>` | `-0.128532`                              | true     |
| time_zone       | The Inventory Time Zone                                                          | `<String>` | `"Europe/London"`                        | true     |
| status          | The Inventory Status. See Status Reference                                       | `<Number>` | `1`                                      | true     |
| private         | Boolean value, when true the Inventory can only be targeted by the account owner | `<Number>` | `true`                                   | false    |
| dwell_interval  | This is the amount of time (in seconds) before a dwell based Event is triggered. | `<Number>` | `300`                                    | false    |
| external_id     | An arbitrary ID to associate to the Inventory                                    | `<String>` | `"external_inv1"`                        | false    |
| place_id        | The associate Place ID                                                           | `<Number>` | 12                                       | false    |
| ttl             | TTL is the maximum value in seconds an event triggered on the inventory should be stored on the device before sending to evt, if the value is 0 then the TTL is indefinate | `<Number>` | 300                                      | false |
| *Beacon*        |                                                                                  |            |                                          |          |
| hover_interval  | This is the amount of time (in seconds) before a hover based Event is triggered. | `<Number>` | `300`                                    | false    |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` | true     |
| major           | The major value of the Beacon.                                                   | `<Number>` | `7`                                      | true     |
| minor           | The minor value of the Beacon.                                                   | `<Number>` | `1`                                      | true     |
| *Eddystone*     |                                                                                  |            |                                          |          |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` | true     |
| *Geofence*      |                                                                                  |            |                                          |          |
| radius          | In meters                                                                        | `<Number>` | `50`                                     | true     |
| *WiFi*          |                                                                                  |            |                                          |          |
| ssid            | The Service Set Identifier                                                       | `<Number>` | `HotSpot 123`                            | true     |
| mac             | The Mac address of the hardware                                                  | `<String>` | `"a1b2c3d4e5f6"`                         | false    |
| *QR/NFC*        |                                                                                  |            |                                          |          |
| destination_url | Default destination url                                                          | `<String>` | `"https://example.com/"`                 | true     |

**Response Attributes**

| Attribute       | Description                                                                      | Type       | Example                                  |
| ---------       | -----------                                                                      | ---        | -------                                  |
| account_id      | The associated Account by ID                                                     | `<Number>` | `101`                                    |
| id              | The unique ID                                                                    | `<Number>` | `279657`                                 |
| type            | The Inventory Type. See Type Reference                                           | `<Number>` | `1`                                      |
| name            | The Inventory Name                                                               | `<String>` | `"Inventory1"`                           |
| latitude        | The Inventory Latitude                                                           | `<Number>` | `51.518908`                              |
| longitude       | The Inventory Longitude                                                          | `<Number>` | `-0.128532`                              |
| time_zone       | The Inventory Time Zone                                                          | `<String>` | `"Europe/London"`                        |
| status          | The Inventory Status. See Status Reference                                       | `<Number>` | `1`                                      |
| private         | Boolean value, when true the Inventory can only be targeted by the account owner | `<Number>` | `true`                                   |
| dwell_interval  | This is the amount of time (in seconds) before a dwell based Event is triggered. | `<Number>` | `300`                                    |
| external_id     | An arbitrary ID to associate to the Inventory                                    | `<String>` | `"external_inv1"`                        |
| place_id        | The associate Place ID                                                           | `<Number>` | 12                                       |
| ttl             | TTL is the maximum value in seconds an event triggered on the inventory should be stored on the device before sending to evt, if the value is 0 then the TTL is indefinate | `<Number>` | 300                                      |
| *Beacon*        |                                                                                  |            |                                          |
| hover_interval  | This is the amount of time (in seconds) before a hover based Event is triggered. | `<Number>` | `300`                                    |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` |
| major           | The major value of the Beacon.                                                   | `<Number>` | `7`                                      |
| minor           | The minor value of the Beacon.                                                   | `<Number>` | `1`                                      |
| *Eddystone*     |                                                                                  |            |                                          |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` |
| *Geofence*      |                                                                                  |            |                                          |
| radius          | In meters                                                                        | `<Number>` | `50`                                     |
| *WiFi*          |                                                                                  |            |                                          |
| ssid            | The Service Set Identifier                                                       | `<Number>` | `HotSpot 123`                            |
| mac             | The Mac address of the hardware                                                  | `<String>` | `"a1b2c3d4e5f6"`                         |
| *QR/NFC*        |                                                                                  |            |                                          |
| destination_url | Default destination url                                                          | `<String>` | `"https://example.com/"`                 |
| code            | Unique random generated 10 char code                                             | `<String>` | `"orsw43dfor2gk4tt"`                     |

**Example**

```
curl -s \
  'https://inv-api.tamoco.com/v1/inventory?account_id=10' \
  -H "Content-Type: application/json" \
  -H 'authorization: Bearer **********' \
  -X POST \
  -d '{"type": 2,"name": "GeoFence","latitude": "0.1","longitude": "0.1","time_zone": "Europe/London","status": 1,
  "private": false,"dwell_interval": 300,"external_id": "external2","radius": 50}'
=>
HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8


'{"id":279657,"account_id":10,"name":"100 A Heineken test office","latitude":51.518908,"longitude":-0.128532,"time_zone":"Europe/London","status":1,"dwell_time":900,"hover_time":900,"radius":100,"type":2}'

```


## Update Inventory

```
PUT /v1/inventory/:id?account_id=:account_id
```

| Attribute       | Description                                                                      | Type       | Example                                  | Required |
| ---------       | -----------                                                                      | ---        | -------                                  | ---      |
| account_id      | The associated Account ID                                                        | `<Number>` | `1`                                      | true     |
| id              | The unique ID                                                                    | `<Number>` | `1`                                      | true     |
| type            | The Inventory Type. See Type Reference                                           | `<Number>` | `1`                                      | true     |
| name            | The Inventory Name                                                               | `<String>` | `"Inventory1"`                           | true     |
| latitude        | The Inventory Latitude                                                           | `<Number>` | `51.518908`                              | true     |
| longitude       | The Inventory Longitude                                                          | `<Number>` | `-0.128532`                              | true     |
| time_zone       | The Inventory Time Zone                                                          | `<String>` | `"Europe/London"`                        | true     |
| status          | The Inventory Status. See Status Reference                                       | `<Number>` | `1`                                      | true     |
| private         | Boolean value, when true the Inventory can only be targeted by the account owner | `<Number>` | `true`                                   | false    |
| dwell_interval  | This is the amount of time (in seconds) before a dwell based Event is triggered. | `<Number>` | `300`                                    | false    |
| external_id     | An arbitrary ID to associate to the Inventory                                    | `<String>` | `"external_inv1"`                        | false    |
| place_id        | The associate Place ID                                                           | `<Number>` | 12                                       | false    |
| ttl             | TTL is the maximum value in seconds an event triggered on the inventory should be stored on the device before sending to evt, if the value is 0 then the TTL is indefinate | `<Number>` | 300                                      | false |
| *Beacon*        |                                                                                  |            |                                          |          |
| hover_interval  | This is the amount of time (in seconds) before a hover based Event is triggered. | `<Number>` | `300`                                    | false    |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` | true     |
| major           | The major value of the Beacon.                                                   | `<Number>` | `7`                                      | true     |
| minor           | The minor value of the Beacon.                                                   | `<Number>` | `1`                                      | true     |
| *Eddystone*     |                                                                                  |            |                                          |          |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` | true     |
| *Geofence*      |                                                                                  |            |                                          |          |
| radius          | In meters                                                                        | `<Number>` | `50`                                     | true     |
| *WiFi*          |                                                                                  |            |                                          |          |
| ssid            | The Service Set Identifier                                                       | `<Number>` | `HotSpot 123`                            | true     |
| mac             | The Mac address of the hardware                                                  | `<String>` | `"a1b2c3d4e5f6"`                         | false    |
| *QR/NFC*        |                                                                                  |            |                                          |          |
| destination_url | Default destination url                                                          | `<String>` | `"https://example.com/"`                 | true     |

**Response Attributes**

| Attribute       | Description                                                                      | Type       | Example                                  |
| ---------       | -----------                                                                      | ---        | -------                                  |
| account_id      | The associated Account by ID                                                     | `<Number>` | `101`                                    |
| id              | The unique ID                                                                    | `<Number>` | `279657`                                 |
| type            | The Inventory Type. See Type Reference                                           | `<Number>` | `1`                                      |
| name            | The Inventory Name                                                               | `<String>` | `"Inventory1"`                           |
| latitude        | The Inventory Latitude                                                           | `<Number>` | `51.518908`                              |
| longitude       | The Inventory Longitude                                                          | `<Number>` | `-0.128532`                              |
| time_zone       | The Inventory Time Zone                                                          | `<String>` | `"Europe/London"`                        |
| status          | The Inventory Status. See Status Reference                                       | `<Number>` | `1`                                      |
| private         | Boolean value, when true the Inventory can only be targeted by the account owner | `<Number>` | `true`                                   |
| dwell_interval  | This is the amount of time (in seconds) before a dwell based Event is triggered. | `<Number>` | `300`                                    |
| external_id     | An arbitrary ID to associate to the Inventory                                    | `<String>` | `"external_inv1"`                        |
| place_id        | The associate Place ID                                                           | `<Number>` | 12                                       |
| ttl             | TTL is the maximum value in seconds an event triggered on the inventory should be stored on the device before sending to evt, if the value is 0 then the TTL is indefinate | `<Number>` | 300                                      |
| *Beacon*        |                                                                                  |            |                                          |
| hover_interval  | This is the amount of time (in seconds) before a hover based Event is triggered. | `<Number>` | `300`                                    |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` |
| major           | The major value of the Beacon.                                                   | `<Number>` | `7`                                      |
| minor           | The minor value of the Beacon.                                                   | `<Number>` | `1`                                      |
| *Eddystone*     |                                                                                  |            |                                          |
| uuid            | The Universally Unique Identifier.                                               | `<String>` | `"b23a41ba-867a-11e7-bb31-be2e44b06b34"` |
| *Geofence*      |                                                                                  |            |                                          |
| radius          | In meters                                                                        | `<Number>` | `50`                                     |
| *WiFi*          |                                                                                  |            |                                          |
| ssid            | The Service Set Identifier                                                       | `<Number>` | `HotSpot 123`                            |
| mac             | The Mac address of the hardware                                                  | `<String>` | `"a1b2c3d4e5f6"`                         |
| *QR/NFC*        |                                                                                  |            |                                          |
| destination_url | Default destination url                                                          | `<String>` | `"https://example.com/"`                 |
| code            | Unique random generated 10 char code                                             | `<String>` | `"orsw43dfor2gk4tt"`                     |

**Example**

```
curl -s \
  'https://inv-api.tamoco.com/v1/inventory/279657?account_id=10' \
  -H 'Content-Type: application/json' \
  -H 'authorization: Bearer ************' \
  -X POST \
  -d '{"id": 12, "type": 2, "name": "GeoFence", "latitude": "0.1", "longitude": "0.1", "time_zone": "Europe/London", "status": 1, "private": false, "dwell_interval": 300, "external_id": "external2", "radius": 50 }'
=>
HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8


{"id":279657,"account_id":10,"name":"100 A Heineken test office","latitude":51.518908,"longitude":-0.128532,"time_zone":"Europe/London","status":1,"dwell_time":900,"hover_time":900,"radius":100,"type":2}

```


## Upload Inventory File

```
POST /v1/inventory/upload/:upload_type?account_id=:account_id
```

| Attribute   | Description                 | Type       | Example        | Required |
| ---------   | -----------                 | ---        | -------        | ---      |
| account_id  | The associated Account ID   | `<Number>` | `1`            | true     |
| upload_type | The upload type, see below  | `<String>` | `geofence`     | true     |
| file        | Tab Separate inventory file | `<File>`   | `geofence.tsv` | true     |

The URL will needs to include `:upload_type`. It can be one of the below depending on the file type:

* beacon
* eddystone
* geofence
* nfc
* qr
* wifi

**Beacon File Format**

```
name	latitude	longitude	dwell_interval	time_zone	private	external_id	uuid	major	minor	hover_interval
inv_1	51.5247	-0.0836	50	Europe/London	true	ext1	0ffdbc6c-28a5-11e6-b67b-9e71128cae77	50	70	100
```

**Eddystone File Format**

```
name	latitude	longitude	dwell_interval	time_zone	private	external_id	hover_interval	uuid
inv_1	51.5247	-0.0836	50	Europe/London	true	ext_1	50	0ffdbc6c-28a5-11e6-b67b-9e71128cae77

```

**Geofence**

```
name	latitude	longitude	dwell_interval	time_zone	private	external_id	radius
inv_1	51.5247	-0.0836	50	Europe/London	true	ext_1	50
```

**NFC**

```
name	latitude	longitude	dwell_interval	time_zone	private	external_id	radius	destination_url
inv_1	51.5247	-0.0836	50	Europe/London	true	ext_1	50	http://www.example.com
```

**QR**

```
name	latitude	longitude	dwell_interval	time_zone	private	external_id	radius	destination_url
inv_1	51.5247	-0.0836	50	Europe/London	true	ext_1	50	http://www.example.com
```

**WiFi**

```
name	latitude	longitude	dwell_interval	time_zone	private	external_id	ssid	mac
inv_1	51.5247	-0.0836	50	Europe/London	true	ext_1	ssid123xyz	a1b2c3d4e5f6
```

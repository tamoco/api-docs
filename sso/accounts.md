# Accounts

An Account groups together users. All resources are scoped to an Account so users can only see resources for the Accounts they are assigned too.

## List Accounts

```
GET /api/v1/accounts
```

**Response Attributes**

| Attribute         | Description          | Type       | Example  |
| ---------         | -----------          | ---        | -------  |
| account["name"]   | The Account name     | `<String>` | Acme     |
| account["domain"] | The Account's domain | `<String>` | acme.com |

**Example**

```
curl -sG \
  https://sso.tamoco.com/api/v1/accounts.json \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer **********"
=> [{"id": 1}]
```

## Create a new Account

When an Account is created the Accounts owner will be set to the creator. A client user can currently create up to 5 Accounts.

```
POST /api/v1/accounts
```

**Params**

| Attribute         | Description          | Type       | Example      | Required |
| ---------         | -----------          | ---        | -------      | ---      |
| account["name"]   | The Account name     | `<String>` | `"Acme"      | true     |
| account["domain"] | The Account's domain | `<String>` | `"acme.com"` | true     |


**Example**

```
curl -s \
  https://sso.tamoco.com/api/v1/accounts.json \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer **********' \
  -X POST \
  -d '{"account": {"name":"name", "domain":"name.com"}}'
=> {"id": 1}
```

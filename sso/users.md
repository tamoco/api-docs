# Users

## Create a new User

```
POST /p.json
```

**Params**

| Attribute                        | Description              | Type       | Example                | Required |
| ---------                        | -----------              | ----       | -------                | -----    |
| partner["name"]                  | User's name              | `<String>` | `"John Doe"`           | true     |
| partner["email"]                 | User's email             | `<String>` | `"john.doe@email.com`" | true     |
| partner["password"]              | User's password          | `<String>` | `"abc123xyz`"          | true     |
| partner["password_confirmation"] | Confirmation of password | `<String>` | `"abc123xyz`"          | true     |


**Example**

```
curl -s \
  'http://localhost:3000/p/' \
  -H "Content-Type: application/json" \
  -X POST \
  -d '{"partner": {"name":"name", "email":"john.doe@email.com", "password": "password", "password_confirmation": "password"}}'
=> {"id":6,"email":"john.doe@email.com","created_at":"2017-06-30T13:14:10.813Z","updated_at":"2017-06-30T13:14:10.813Z","name":"name","account_id":null,"admin":false,"authentication_token":null}
```

## Get a User

You can only get the details for the current logged in User.

```
GET /me.json
```

**Response Attributes**

| Attribute            | Description                       | Type              | Example                  |
| ---------            | -----------                       | -----             | -------                  |
| id                   | The user's ID                     | `<Integer>`       | 16                       |
| email                | The user's email                  | `<String>`        | john.doe@email.com       |
| created_at           | The date user was created         | `<Datetime>`      | 2016-06-15T13:04:15.430Z |
| updated_at           | The date user was created         | `<Datetime>`      | 2016-06-15T13:04:15.430Z |
| type                 | The user's type                   | `<String>`        | Partner,Employee         |
| name                 | The user's name                   | `<String>`        | John Doe                 |
| roles                | The users roles                   | `<Array<String>>` | ['a','b']                |
| accounts             | The user's associated accounts    | `<Array<Hash>>`   | [{},{}]                  |
| accounts[id]         | The account's ID                  | `<Integer>`       | 12                       |
| accounts[name]       | The account's name                | `<String>`        | Acme                     |
| accounts[updated_at] | The date account was last updated | `<Datetime>`      | 2016-06-15T13:04:15.430Z |
| accounts[created_at] | The date account was created      | `<Datetime>`      | 2016-06-15T13:04:15.430Z |
| accounts[sdk_secret] | Deprecated DNU                    | N/A               | N/A                      |
| accounts[domain]     | The account's domain              | `<String>`        | acme.com                 |

**Example**

```
curl -sG \
  https://sso.tamoco.com/me.json \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer *************"
=> {"id":16,"email":"user@gmail.com","created_at":"2016-06-15T13:04:15.430Z","updated_at":"2017-07-19T15:04:12.467Z","type":"Partner","name":"John Doe","roles":[],"accounts":[{"id":1,"name":"Fleetwood Mac","created_at":"2016-04-29T12:44:45.107Z","updated_at":"2016-05-03T15:47:09.844Z","sdk_secret":"a19bf84d7c4ca393f0cae433ff44671bc7833adc","domain":"fleetwoodmac.com"}],"account_id":1}

```
